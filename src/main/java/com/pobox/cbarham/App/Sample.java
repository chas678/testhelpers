package com.pobox.cbarham.App;

/**
 * Class to...
 *
 * @author chas
 *         Date: 16/02/13
 */

public interface Sample {
    String getName();

    void setName(String name);

    void setOtherSample(Sample sample);
}

